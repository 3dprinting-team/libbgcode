% bgcode(1) | User Commands
%
% "December  23 2023"

# NAME

bgcode - compress/decompress byte streams using LZSS

# SYNOPSIS

**bgcode** [_IN\_FILE_] [ OPTIONS ]

# DESCRIPTION

**bgcode** converts textual gcode files into binary gcode, as used by Prusa 3D printers.

# OPTIONS

**\-\-checksum**=_CHECKSUM_
:   Checksum algorithm to use. Valid values for _CHECKSUM_ are `0` for no
    checksum and `1` for CRC32 (default).

**\-\-file_metadata_compression**=_COMPRESSION_
:   Compression algorithm to use for file metadata. Valid values for
    _COMPRESSION_ are: `0` for None (default), `1` for Deflate, `2` for
    Heatshrink\_11\_4, and `3` for Heatshrink\_12\_4.

**\-\-print_metadata_compression**=_COMPRESSION_
:   Compression algorithm to use for print metadata. Valid values for
    _COMPRESSION_ are: `0` for None (default), `1` for Deflate, `2` for
    Heatshrink\_11\_4, and `3` for Heatshrink\_12\_4.

**\-\-printer_metadata_compression**=_COMPRESSION_
:   Compression algorithm to use for printer metadata. Valid values for
    _COMPRESSION_ are: `0` for None (default), `1` for Deflate, `2` for
    Heatshrink\_11\_4, and `3` for Heatshrink\_12\_4.

**\-\-slicer_metadata_compression**=_COMPRESSION_
:   Compression algorithm to use for slicer metadata. Valid values for
    _COMPRESSION_ are: `0` for None (default), `1` for Deflate, `2` for
    Heatshrink\_11\_4, and `3` for Heatshrink\_12\_4.

**\-\-gcode_compression**=_COMPRESSION_
:   Compression algorithm to use for G-code. Valid values for
    _COMPRESSION_ are: `0` for None (default), `1` for Deflate, `2` for
    Heatshrink\_11\_4, and `3` for Heatshrink\_12\_4.

**\-\-gcode_encoding**=_GCODE\_ENCODING_
:   Encoding format to use for G-code. Valid values for _GCODE\_ENCODING_ are
    `0` for None (default), `1` for MeatPack, and `2` for MeatPackComments.

**\-\-metadata_encoding**=_METADATA\_ENCODING_
:   Encoding format to use for metadata. The only available value for this is
    `0` for INI (default).

# BUGS

The upstream BTS can be found at https://github.com/prusa3d/libbgcode/issues.

# AUTHOR

Chow Loong Jin <hyperair@debian.org>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2023 Chow Loong Jin

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

[comment]: #  Local Variables:
[comment]: #  mode: markdown
[comment]: #  End:
